
import java.util.*;

public class App implements  BangunRuang{
		
	int p3 =3;
	int p2 =2;
	
	int phi1 = 22;
	int phi2 =7;
	
	double a=3;
	double b=1/a;

	double c=4/a;
	
	double volume;

	// Fungsi Nama
	public void getNama(String nama){

		System.out.println("============================================");
		System.out.println("Rumus Bangun Ruang "+nama);
		System.out.println("Nama :"+nama);
				
	}

	// Fungsi Warna
    	public void getWarna(String warna){
		System.out.println("Warna :"+warna);
	}

 	// Fungsi Volume yang akan mengimplementasikan overload
	// rumus kubus
		public void  getVolume(int s){ 
 		volume= Math.pow(s, p3);
		System.out.println("Sisi :"+ s);
		System.out.println("V :"+volume);
	}

	//rumus kerucut
	public void  getVolume(int r,int t){ 
 		double r2= Math.pow(r,p2);
		volume=b*phi1/phi2*r2*t;
		System.out.println("r :"+ r);
		System.out.println("t :"+ t);
		System.out.println("V :"+volume);
	}
	

	//rumus bola
	public void  getVolume(double r){ 
 		double r3= Math.pow(r,p3);	
		volume=c*phi1/phi2*r3;
		System.out.println("r :"+ r);
		System.out.println("V :"+volume);
	}

 	public static void main(String[] args){

		App result= new App();

		//Menampilkan hasil rumus kubus
		result.getNama("KUBUS");
		result.getWarna("Merah");
		result.getVolume(10);

		//Menampilkan hasil rumus kerucut
		result.getNama("KERUCUT");
		result.getWarna("Hijau");
		result.getVolume(7,3);

		//Menampilkan hasil rumus kerucut
		result.getNama("Bola");
		result.getWarna("Magenta");
		result.getVolume(3.5);

	
	}

}
